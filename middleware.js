const express = require('express');
const app = express();
const fetch = require('node-fetch');

app.get('/api/middleware', (req, res ) => {
   
   fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(json => {
        res.send(json);
    });
   
});

let myLogger = function (req, res, next) {
    // does something by load order and points to the next (physically) function
    console.log('LOGGED');
    req.requestTime = Date.now();
    next();
};
// it will be executed before the next listener, don't take effect for /api/middleware because it's set before than this middleware  
app.use(myLogger);

app.get('/api/middleware2', (req, res ) => {
    fetch('https://dog-facts-api.herokuapp.com/api/v1/resources/dogs?number=1')
    .then(res => res.json())
    .then(json => {
        // do stuff with data 
        console.log(req.requestTime);
        res.send(json);
    })
    .catch(err => console.error(err));
});

/* app.post('/api/middleware', (req, res) => {
    fetch('https://httpbin.org/post', {
        method: 'post',
        body:    JSON.stringify(req.body),
        headers: { 'Content-Type': 'application/json' },
    })
    .then(res => res.json())
    .then(json => console.log(json))
    .catch(err => console.error(err));

    res.send(res);
}) */;

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));