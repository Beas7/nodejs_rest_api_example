const Joi = require('joi');
const express = require('express');
const app = express();
// to enable parsing of json objects in the body of the requests:
app.use(express.json());

const courses = [
    { id: 1, name: 'First Course 1' },
    { id: 2, name: 'Second Course 2' },
    { id: 3, name: 'Third Course 3' },
];

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.get('/api/courses', (req, res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) => {
    const course = courseExists(req.params.id);
    if (!course) return res.status(404).send('The course with the given ID was not found');

    res.send(course);
});

app.post('/api/courses/', (req, res) => {
    // destructuring, it takes error property inside req.body and expose it to error variable
    const { error } = validateCourse(req.body);
    if (error) {
        let errorMessage = '';
        // if there's any error we iterate over all of them and concatenate its messages
        error.details.forEach( err => {
            errorMessage += err.message
        });
        // we send a status (400 - Bad request) by convention along the error message/s
        return res.status(400).send( errorMessage );
    }

    const course = {
        // if there were a database it would take care for the id autoincrement, here we add it manually
        id: ++courses.length,
        // also we should get the name posted inside the body of the request
        name: req.body.name
    };

    courses.push(course);
    res.send(course);
});

app.put('/api/courses/:id', (req, res) => {
    const course = courseExists(req.params.id);
    if (!course) return res.status(404).send('The course with the given ID was not found');
    
    const { error } = validateCourse(req.body);

    if (error) {
        let errorMessage = '';
        error.details.forEach( err => {
            errorMessage += err.message
        });
        return res.status(400).send( errorMessage );
    }

    course.name = req.body.name;
    res.send(course);
});


app.delete('/api/courses/:id', (req, res) => {
    const course = courseExists(req.params.id);
    if (!course) return res.status(404).send('The course with the given ID was not found');

    const index = courses.indexOf(course);
    courses.splice(index, 1);

    res.send(course);
});

// RAW URL DATA: We can also get raw data from the route like this: (use /api/posts/2018/3 to see the result)
app.get('/api/posts/:year/:month', (req, res) => {
    res.send(req.params);
});

// QUERY STRING PARAMS: Or queryString like this /api/posts?sortBy=Name <- then you'll see the query param sortBy and the value name)
app.get('/api/posts/', (req, res) => {
    res.send(req.query);
});

function validateCourse(course) {
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    });
    return schema.validate(course);
}

function courseExists( id ) {
    const course = courses.find( c => c.id === parseInt(id) );
    return course;
}


// if env.port is set we use this, otherwise we use 3000
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));

// TODO: if this were a real project we should create a courses.js with all the stuff related to courses API 