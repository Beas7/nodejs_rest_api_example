# NodeJS_REST_API_Example

**REST operations**
* GET /api/courses
* GET /api/courses/:id { 'id' : required }
* POST /api/courses { 'name' : String > 3, Required }
* PUT /api/courses/:id { 'id' : required }
* DELETE /api/courses/:id { 'id' : required }


# Dependencies
* Node ^15.9.0
* Express : https://expressjs.com/es/4x/api.html
* Joi : https://joi.dev/api/?v=17.4.0

Project initial setup: 

* npm init -y
* npm install express
* npm install joi
* sudo npm i -g nodemon

To export a PORT (set value inside this env var), let's say 5000 we can use (on linux)
  * export PORT=5000

If you are on windows powerShell it is:
 * set PORT=5000


 **Nodemon** is a watcher for NodeJS. It detects changes and rerun the server so you can avoid stopping and launching the server several times while coding and testing.

 _____
# How to run?
- npm install
- nodemon app.js



 You can see the project using http://localhost:PORT/api/courses?...

 if you don't set the port, the default is 3000, so to retrieve all courses would be a GET request to:
 http://localhost:3000/api/courses